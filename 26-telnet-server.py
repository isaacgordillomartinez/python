# /usr/bin/python3
#-*- coding: utf-8-*-
import sys, socket, os, signal, argparse
from subprocess import Popen, PIPE

'''
def senyal1(signum, frame):
    print("Lista de conexiones:")
    print(lista_conexiones)
    print("Vaciando lista de conexiones")
    lista_conexiones = []
'''
    
def senyal10(signum, frame):
    print("Lista de conexiones:")
    print(lista_conexiones)

def senyal12(signum, frame):
    print("Cantidad total de conexiones:")
    print(contador_conexiones)
    sys.exit(0)

def senyal15(signum, frame):
    print("Lista de conexiones:")
    print(lista_conexiones)
    print("Cantidad total de conexiones:")
    print(contador_conexiones)

HOST = ''

parser = argparse.ArgumentParser(
    description="Servidor telnet")

parser.add_argument(
    "-d", "--debug", type=bool,
    dest="debug",
    choices=[True|False],
    help="Modo debug.")

parser.add_argument(
    "-p", "--port", type=int,
    default="50001",
    help="Puerto al que asignar el servicio")

args=parser.parse_args()

DEBUG = args.debug

#signal.signal(signal.SIGHUP, senyal1)
signal.signal(signal.SIGUSR1, senyal10)
signal.signal(signal.SIGUSR2, senyal12)
signal.signal(signal.SIGTERM, senyal15)

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

s.bind((HOST, args.port))

s.listen(1)

pid = os.fork()

if pid != 0:
    print("Terminando programa padre.")
    sys.exit(0)

print("Servidor telnet encendido:", os.getpid(), pid)

lista_conexiones = []
contador_conexiones = 0

while True:
    conn, addr = s.accept()

    print("Cliente conectado:", addr)

    lista_conexiones.append(addr)
    contador_conexiones += 1

    while True:
        orden = conn.recv(1024)
        if not orden:
            print("No hay datos recibidos")
            break

        pipeData = Popen(orden, shell=True, stdout=PIPE, stderr=PIPE)
        
        if DEBUG:
            print(f'debug: Orden ejecutada: {orden}')

        for line in pipeData.stdout:
            conn.sendall(bytes(str(line), 'utf-8'))
            if DEBUG:
                sys.stdout.write(str(line,'utf-8'))
        conn.sendall(bytes("4", 'utf-8'))

    conn.close()
s.close()
sys.exit(0)
